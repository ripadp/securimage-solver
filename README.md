Securimage Solver
=================

Setup
-----

1. Install PHP 5.4+ and the PHP-GD extension (on Arch: `sudo pacman -S php php-gd`)
2. In `/etc/php/php.ini`, find the line saying `extension=gd` and make sure it's uncommented (to activate PHP-GD)
3. `git submodule init && git submodule update --recursive` to pull the Securimage source code in this repo

Current status
--------------

`php generate-secureimage.php` will generate a training image, save it to the `images/` folder, name the file with the solution code, and print the filename it just saved on stdout. We'll use this to create a training dataset.

Next steps
----------

- Define a model to process the images
- Create a test dataset
- Train!

Head over to the [wiki](https://gitlab.com/ripadp/securimage-solver/wikis/home) for resources, inspirations, and the current status on development.
